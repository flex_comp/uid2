module gitlab.com/flex_comp/uid2

go 1.17

require (
	github.com/sony/sonyflake v1.0.0
	gitlab.com/flex_comp/comp v0.1.4
	gitlab.com/flex_comp/log v0.1.6
)

require (
	github.com/json-iterator/go v1.1.11 // indirect
	github.com/modern-go/concurrent v0.0.0-20180228061459-e0a39a4cb421 // indirect
	github.com/modern-go/reflect2 v1.0.1 // indirect
	github.com/spf13/cast v1.4.0 // indirect
	gitlab.com/flex_comp/util v0.0.0-20210729132803-de11a044b5ed // indirect
	go.uber.org/atomic v1.7.0 // indirect
	go.uber.org/multierr v1.6.0 // indirect
	go.uber.org/zap v1.18.1 // indirect
	gopkg.in/natefinch/lumberjack.v2 v2.0.0 // indirect
)
