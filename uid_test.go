package uid

import (
	"gitlab.com/flex_comp/comp"
	"gitlab.com/flex_comp/log"
	"testing"
	"time"
)

func TestUID_Next(t *testing.T) {
	t.Run("uid", func(t *testing.T) {
		_ = comp.Init(map[string]interface{}{
			"env":      "debug",
			"log.path": "./test/test.log",
			"log.size": 50,
			"log.age":  1,
		})

		ch := make(chan bool, 1)
		go func() {
			ticker := time.NewTicker(time.Millisecond)
			close := time.NewTimer(time.Second * 10)

			defer func() {
				ticker.Stop()
			}()

			for {
				select {
				case <-ticker.C:
					log.Debug(Next())
				case <-close.C:
					ch <- true
					return
				}
			}

		}()

		_ = comp.Start(ch)
	})
}
