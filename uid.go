package uid

import (
	"github.com/sony/sonyflake"
	"gitlab.com/flex_comp/comp"
	"gitlab.com/flex_comp/log"
	"sync"
	"time"
)

var (
	ins *UID
)

func init() {
	ins = new(UID)
	_ = comp.RegComp(ins)
}

type UID struct {
	group sync.Map
}

func (u *UID) Init(map[string]interface{}, ...interface{}) error {
	return nil
}

func (u *UID) Start(...interface{}) error {
	return nil
}

func (u *UID) UnInit() {
}

func (u *UID) Name() string {
	return "uid2"
}

// Next 生成一个唯一ID,如果传入参数,则作为分组并行生成(有几率重复)
// sonyflake 库最多支持每10ms生成256个uid,如果溢出则等待; 使用此
// 库最大的理由在于其支持无需填入machine(node)ID, 不填machineID
// 的情况下会去找私有ip后段(a.a.b.b中的b)来自动生成machineID,所以
// 请保证所有需要保证唯一性的服务在同一网段下(如:192.168.0.0-192.168.255.255)
func Next(group ...string) uint64 {
	return ins.Next(group...)
}

func (u *UID) Next(group ...string) uint64 {
	name := "default"
	if len(group) > 0 {
		name = group[0]
	}

	sf, _ := u.group.LoadOrStore(name, sonyflake.NewSonyflake(sonyflake.Settings{StartTime: time.UnixMilli(0)}))
	id, err := sf.(*sonyflake.Sonyflake).NextID()
	if err != nil {
		log.Error(err)
	}

	return id
}
